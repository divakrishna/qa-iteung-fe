import { configureStore } from '@reduxjs/toolkit';

import notesReducer from './features/notes/notesSlice';
import userReducer from './features/user/userSlice';
import classementReducer from './features/classement/classementSlice';

export default configureStore({
  reducer: {
    classement: classementReducer,
    notes: notesReducer,
    user: userReducer
  }
});