import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

const user = JSON.parse(localStorage.getItem('user'));

const initialState = {
  data: [],
  status: 'idle',
  error: null
};

export const fetchClassement = createAsyncThunk('classement/fetchClassement', async () => {
  const requestOptions = {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${user.token}`,
      'Content-Type': 'application/json'
    }
  };

  const response = await fetch(`${process.env.REACT_APP_API_URL}/classement`, requestOptions);
  if (response.ok) {
    const data = await response.json();
    return data;
  } else {
    throw Error(response.statusText);
  }
});

const classementSlice = createSlice({
  name: 'classement',
  initialState,
  reducers: {
    statusReset(state, action) {
      state.status = 'idle';
    }
  },
  extraReducers: {
    [fetchClassement.pending]: (state, action) => {
      state.status = 'loading';
    },
    [fetchClassement.fulfilled]: (state, action) => {
      state.status = 'succeeded';
      state.data = action.payload;
    },
    [fetchClassement.rejected]: (state, action) => {
      state.status = 'failed';
      state.error = action.error.message;
    },
  }
});
export const getClassement = (state) => state.classement.data;

export const { statusReset } = classementSlice.actions;

export default classementSlice.reducer;
