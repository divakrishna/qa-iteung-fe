/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { unwrapResult } from '@reduxjs/toolkit';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/free-solid-svg-icons';
import { Form, FormGroup, FormButtonGroup, TextAreaQ, TextAreaA } from './ui/Form';
import Button from './ui/Button';
import Message from './ui/Message';
import { addNewNote, statusReset } from '../features/notes/notesSlice';

const InfoWrapper = (props) => {
  const { status } = props;

  if (status !== null) {
    if (status === false) {
      return <Message type='error' text='Pertanyaan tidak boleh kosong' />;
    }
    return <Message type='success' text='Data berhasil disimpan' />;
  }
  return <></>;
};

const AddNoteForm = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    question: '',
    answer: '',
  });
  const [isSuccess, setIsSuccess] = useState(null);

  const handleTitleChange = (e) => {
    setState({ ...state, question: e.target.value });
  };

  const handleNoteChange = (e) => {
    setState({ ...state, answer: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const actionResult = await dispatch(addNewNote(state));
      const result = unwrapResult(actionResult);
      if (result) {
        setIsSuccess(true);
        history.push('/');
      } else {
        setIsSuccess(false);
      }
    } catch (err) {
      console.error('Terjadi kesalahan: ', err);
      setIsSuccess(false);
    } finally {
      dispatch(statusReset());
    }
  };

  const { question, answer } = state;

  return (
    <>
      <InfoWrapper status={isSuccess} />
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <TextAreaQ
            rows='6'
            name='question'
            placeholder='Pertanyaan atau pernyataan yang disampaikan.. Misalnya: selamat pagi beb..'
            value={question}
            onChange={handleTitleChange}
          />
        </FormGroup>
        <FormGroup>
          <TextAreaA
            name='answer'
            rows='6'
            placeholder='Tanggapan yang harusnya diberikan sesuai pertanyaan atau pernyataan diatas.. Misalnya: selamat pagi juga sayang..'
            value={answer}
            onChange={handleNoteChange}
          />
        </FormGroup>
        <FormButtonGroup>
          <Button type='submit'>
            <FontAwesomeIcon icon={faSave} /> &nbsp; Simpan
          </Button>
        </FormButtonGroup>
      </Form>
    </>
  );
};

export default AddNoteForm;
