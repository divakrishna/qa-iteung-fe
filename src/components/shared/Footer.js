import React from "react";
import tw from "twin.macro";

const Container = tw.div`m-4 p-2`;

const Footer = () => {
  return (
    <Container>
      <p>
        ITeung &copy; 2021 - Thanks to <a href="https://devsaurus.com">devsaurus</a>
      </p>
    </Container>
  );
};

export default Footer;
