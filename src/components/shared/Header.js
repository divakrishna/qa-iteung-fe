import React, { useState } from 'react';
import tw from 'twin.macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDoorClosed, faFile, faStar } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import Button from '../ui/Button';
import logo from '../../assets/images/header-logo.png';
import { useDispatch } from 'react-redux';
import { logout } from '../../features/user/userSlice';

const Navigation = tw.div`flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-3`;
const Img = tw.img`h-14 w-auto sm:h-16`;
const Heading = tw.h2`invisible text-xl font-bold text-gray-900 md:visible`;
const Menu = tw.div`md:flex items-center justify-end md:flex-1 lg:w-0`;

const Header = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout());
    window.location.reload();
  };

  return (
    <>
      <Navigation>
      <Link to="/">
        <Img src={logo} alt="logo" />
        </Link>
        <Heading>ITeung Belajar</Heading>
        <Menu>
          <Link to="/add">
            <Button>
              <FontAwesomeIcon icon={faFile} />
              &nbsp;&nbsp; Ajari ITeung
            </Button>
          </Link>
          <Link to="/classement">
            <Button>
              <FontAwesomeIcon icon={faStar} />
              &nbsp;&nbsp; Top Pengajar
            </Button>
          </Link>
          <Button danger onClick={handleLogout}>
              <FontAwesomeIcon icon={faDoorClosed} />
              &nbsp;&nbsp; Keluar
          </Button>
        </Menu>
      </Navigation>
    </>
  );
};

export default Header;
