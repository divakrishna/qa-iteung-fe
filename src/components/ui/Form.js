import tw from "twin.macro";

const Form = tw.form`flex flex-col w-4/5`;

const FormGroup = tw.div`flex flex-col`;

const FormButtonGroup = tw.div`flex justify-end`;

const TextAreaQ = tw.textarea`border mt-8 mb-4 p-2 font-bold focus:outline-none focus:ring focus:border-blue-300 resize-y`;

const TextAreaA = tw.textarea`border resize-y mb-8 p-2 focus:outline-none focus:ring focus:border-blue-300`;

export { Form, FormGroup, FormButtonGroup, TextAreaQ, TextAreaA };
