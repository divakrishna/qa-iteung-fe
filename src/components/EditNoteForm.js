/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
import React, { useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { unwrapResult } from '@reduxjs/toolkit';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Form, FormGroup, FormButtonGroup, TextAreaQ, TextAreaA } from './ui/Form';
import Button from './ui/Button';
import Message from './ui/Message';
import { useSelector, useDispatch } from 'react-redux';

import {
  getNoteById,
  updateExistingNote,
  deleteNote,
  statusReset,
} from '../features/notes/notesSlice';

const InfoWrapper = (props) => {
  const { status } = props;

  if (status !== null) {
    if (status === false) {
      return <Message type='error' text='Pertanyaan tidak boleh kosong' />;
    }
    return <Message type='success' text='Data berhasil disimpan' />;
  }
  return <></>;
};

const EditNoteForm = () => {
  const location = useLocation();
  const history = useHistory();

  const dispatch = useDispatch();

  const noteId = location.pathname.replace('/edit/', '');

  const currentNote = useSelector((state) => getNoteById(state, noteId));

  const [state, setState] = useState(currentNote);
  const [isSuccess, setIsSuccess] = useState(null);

  const handleTitleChange = (e) => {
    setState({ ...state, question: e.target.value });
  };

  const handleNoteChange = (e) => {
    setState({ ...state, answer: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const actionResult = await dispatch(updateExistingNote(state));
      const result = unwrapResult(actionResult);
      if (result) {
        setIsSuccess(true);
        history.push('/');
      } else {
        setIsSuccess(false);
      }
    } catch (err) {
      console.error('Terjadi kesalahan: ', err);
      setIsSuccess(false);
    } finally {
      dispatch(statusReset());
    }
  };

  const handleDeleteNote = async (e) => {
    e.preventDefault();

    try {
      const actionResult = await dispatch(deleteNote(state));
      const result = unwrapResult(actionResult);
      if (result) {
        setIsSuccess(true);
        history.push('/');
      } else {
        setIsSuccess(false);
      }
    } catch (err) {
      console.error('Terjadi kesalahan: ', err);
      setIsSuccess(false);
    } finally {
      dispatch(statusReset());
    }
  };

  const { question, answer } = state;

  return (
    <>
      <InfoWrapper status={isSuccess} />
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <TextAreaQ
            name='question'
            value={question}
            rows='6'
            placeholder='Pertanyaan atau pernyataan yang disampaikan.. Misalnya: selamat pagi beb..'
            onChange={handleTitleChange}
          />
        </FormGroup>
        <FormGroup>
          <TextAreaA
            name='answer'
            rows='6'
            placeholder='Tanggapan yang harusnya diberikan sesuai pertanyaan atau pernyataan diatas.. Misalnya: selamat pagi juga sayang..'
            value={answer}
            onChange={handleNoteChange}
          />
        </FormGroup>
        <FormButtonGroup>
          <Button type='submit'>
            <FontAwesomeIcon icon={faSave} /> &nbsp; Ubah
          </Button>
          <Button danger onClick={handleDeleteNote}>
            <FontAwesomeIcon icon={faTrashAlt} /> &nbsp; Hapus
          </Button>
        </FormButtonGroup>
      </Form>
    </>
  );
};

export default EditNoteForm;
