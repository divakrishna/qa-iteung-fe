/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState, useRef } from 'react';
import tw from 'twin.macro';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortAmountUp } from '@fortawesome/free-solid-svg-icons';
import { fetchNotes, getFilteredNotes, updateSort } from '../features/notes/notesSlice';
import Container from './ui/Container';

const NotesListContainer = tw.div`grid grid-cols-1 md:grid-cols-3 gap-4 my-8 overflow-y-auto`;
const Card = tw.div`text-left p-4 border rounded-md`;
const Title = tw.h4`font-semibold text-purple-900`;
const Information = tw.div` text-left mb-4 w-9/12`
const SearchBar = tw.input`my-4 mr-4 p-2 text-left border rounded focus:outline-none focus:ring focus:border-blue-300 w-full`;
const Toolbar = tw.div`flex flex-row w-9/12 justify-end`;
const DropdownInnerWrapper = tw.div`relative inline-block text-left`;
const SortIcon = tw.button`text-base text-right my-4 p-2 border rounded-md`;
const DropdownPanel = tw.div`origin-top-right absolute right-0 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5`;
const Menu = tw.ul`py-1`;
const Item = tw.li`block px-4 py-2 w-full text-sm text-left text-gray-700 hover:bg-gray-100 hover:text-gray-900 cursor-pointer`;
const Alert = tw.div`text-right my-6 w-10/12 border-r-4  py-2 px-4`
const AlertTitle = tw.p`font-bold`

const DropdownMenu = () => {
  const [visible, setVisible] = useState(false);
  const node = useRef();
  const dispatch = useDispatch();

  useEffect(() => {
    document.addEventListener('mousedown', handleClick);

    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  const handleChange = (e) => {
    setVisible(!visible);
  };

  const handleClick = (e, option) => {
    if (node.current.contains(e.target)) {
      dispatch(updateSort(option));
      return;
    }
    setVisible(false);
  };

  return (
    <DropdownInnerWrapper ref={node}>
      <SortIcon onClick={handleChange}>
        <FontAwesomeIcon icon={faSortAmountUp} size="lg" />
      </SortIcon>
      {visible && (
        <DropdownPanel>
          <Menu role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
            <Item role="menuitem" onClick={(e) => handleClick(e, 'newest')}>
            Tanggal Modifikasi Terbaru 
            </Item>
            <Item role="menuitem" onClick={(e) => handleClick(e, 'oldest')}>
            Tanggal Modifikasi Terlama 
            </Item>
          </Menu>
        </DropdownPanel>
      )}
    </DropdownInnerWrapper>
  );
};

const NotesList = () => {
  const [keyword, setKeyword] = useState('');
  const dispatch = useDispatch();
  const notes = useSelector((state) => getFilteredNotes(state, keyword));
  const notesStatus = useSelector((state) => state.notes.status);
  const error = useSelector((state) => state.notes.error);

  useEffect(() => {
    if (notesStatus === 'idle') {
      dispatch(fetchNotes());
    }
  }, [notesStatus, dispatch]);

  const handleChange = (e) => {
    setKeyword(e.target.value);
  };

  let content;

  if (notesStatus === 'loading') {
    content = <div>Loading...</div>;
  } else if (notesStatus === 'succeeded') {
    console.log(notes)
    if(notes.length > 0){
      content = notes.map((note) => {
        return (
          <Card key={note._id}>
            <Title>
              <Link to={`/edit/${note._id}`}>Pertanyaan : {note.question.slice(0, 101)}</Link>
            </Title>
            <p><b>Tanggapan :</b> {note.answer.slice(0, 101)}</p>
          </Card>
        );
      });
    }else{
    content = <div>Belum ada data yang diajari</div>;
    }
  } else if (notesStatus === 'failed') {
    content = <div>{error}</div>;
  }

  return (
    <Container>
      <Alert>
        <AlertTitle>Total Yang Diajarkan Olehmu</AlertTitle>
        <p style={{fontSize:'25px'}}>{notes ? notes.length:0} kalimat</p>
      </Alert>
      <Information>
      <p style={{fontSize: '20px', fontWeight: '600', marginBottom: '4px'}}>Cara Ngajar:</p>
        <ol style={{listStyleType: 'lower-alpha', marginLeft: '18px'}}>
          <li>Klik tombol "Ajari ITeung".</li>
          <li><b>Masukan pertanyaan atau pernyataan</b>, lalu masukan juga <b>tanggapan</b> dari pernyataan atau pertanyaan.</li>
          <li>Setelah itu, klik "Simpan". Data yang Anda masukan nantinya akan muncul di halaman beranda.</li>
          <li>Apabila ada kesalahan terhadap data yang dimasukan dan ingin <b>mengubahnya</b>, klik data tersebut, lalu Anda diarahkan ke halaman ubah data. Lalu ubah data, kemudian klik "Ubah".</li>
          <li>Apabila ada kesalahan terhadap data yang dimasukan dan ingin <b>menghapusnya</b>, klik data tersebut, lalu Anda diarahkan ke halaman ubah data. Lalu klik "Hapus".</li>
          <li>Lihat peringkatmu di "Top Pengajar".</li>
        </ol>
      </Information>
      <Toolbar>
        <SearchBar placeholder="Pencarian..." onChange={handleChange} />
        <DropdownMenu />
      </Toolbar>
      <NotesListContainer>{content}</NotesListContainer>
    </Container>
  );
};

export default NotesList;
