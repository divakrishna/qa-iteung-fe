/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react';
import tw from 'twin.macro';
import { useSelector, useDispatch } from 'react-redux';
import {fetchClassement, getClassement} from '../features/classement/classementSlice'
import Container from './ui/Container';

const NotesListContainer = tw.div`grid grid-cols-1 md:grid-cols-4 gap-4 my-8 overflow-y-auto`;
const Card = tw.div`text-left p-4 border rounded-md`;
const Title = tw.h4`font-semibold text-purple-900`;
const Order = tw.h4`font-semibold text-right text-2xl`

const ClassementList = () => {
  const dispatch = useDispatch();
  const notes = useSelector((state) => getClassement(state));
  const notesStatus = useSelector((state) => state.classement.status);
  const error = useSelector((state) => state.classement.error);

  const changeCharacter = (sentence) => {
    if(sentence.length>3){
        const random = (Math.random() * 5)+1
        let result = sentence.slice(0,random)
    for (let i=random; i < sentence.length; i++) {
        result += '*'
    }
    return result
    }
    return sentence.slice(0,1)+'*************'
  }

  useEffect(() => {
    if (notesStatus === 'idle') {
      dispatch(fetchClassement());
    }
  }, [notesStatus, dispatch]);

  let content;

  if (notesStatus === 'loading') {
    content = <div>Loading...</div>;
  } else if (notesStatus === 'succeeded') {
    console.log(notes)
    if(notes.length > 0){
      content = notes.map((note, index) => {
        return (
          <Card key={index}>
            <Title>
              {changeCharacter(note._id)}
            </Title>
            <p><b>{note.count}</b> kalimat</p>
            <Order>#{index+1}</Order>
          </Card>
        );
      });
    }else{
    content = <div>Belum ada datanya</div>;
    }
  } else if (notesStatus === 'failed') {
    content = <div>{error}</div>;
  }

  return (
    <Container>
        <h1 style={{fontSize:'30px', fontWeight:700}}>Peringkat Pengajar ITeung</h1>
        <br />
        <p>
          <i>"Jadilah pengajar ITeung karena kata Aristoteles, satu-satunya tanda eksklusif dari pengetahuan yang menyeluruh adalah kekuatan mengajar."</i>
        </p>
        <p>
          <i>"Terima kasih telah mengajari ITeung :)."</i>
        </p>
        <br />
      <NotesListContainer>{content}</NotesListContainer>
    </Container>
  );
};

export default ClassementList;
