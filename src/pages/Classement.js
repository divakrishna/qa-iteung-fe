import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PageLayout from '../layouts/PageLayout';
import ClassementList from '../components/ClassementList';
import Container from '../components/ui/Container';

const ClassementPage = () => {
  const isLoggedIn = useSelector((state) => state.user.isLoggedIn);

  return (
    <>
      {isLoggedIn ? (
        <PageLayout>
          <Container>
            <ClassementList>Notes List</ClassementList>
          </Container>
        </PageLayout>
      ) : (
        <Redirect to="/login" />
      )}
    </>
  );
};

export default ClassementPage;
